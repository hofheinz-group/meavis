class AgilentB596X:
    def __init__(self, host):
        pass

    def conf(self):
        pass

    def output(self, value):
        pass

    def set_channel(self, channel):
        pass

    def set_mode(self, mode):
        pass

    def set_value(self, value):
        pass


class KeySight344XX:
    def __init__(self, host):
        pass

    def calc_average(self):
        pass

    def conf(self):
        pass

    def conf_ACorDC(self, ACorDC):
        pass

    def conf_mode(self, mode):
        pass

    def count(self, value):
        pass

    def initiate(self):
        pass

    def opc(self):
        pass

    def set_aperture(self, value):
        pass
