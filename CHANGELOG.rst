ChangeLog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_ and this project adheres to
`Semantic Versioning`_.

.. _Keep a Changelog: https://keepachangelog.com/
.. _Semantic Versioning: https://semver.org/

Unreleased
----------

Added
^^^^^

Change
^^^^^^

Deprecated
^^^^^^^^^^

Removed
^^^^^^^

Fixed
^^^^^

[0.2.1]
-------

Added
^^^^^

* Basic IV measurement as first example.
* Loop completer for parameters.
* Tags mechanism.

Change
^^^^^^

* Mapping inheritance for MeaVis Markup Language -- Instrument.
* Constructor and Initialiser tags for MeaVis Markup Language -- Instrument.
